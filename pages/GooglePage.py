from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from pages.BasePage import BasePage


class GooglePage(BasePage):
    SEARCH_FIELD = (By.NAME, 'q')
    RESULTS = (By.CSS_SELECTOR, '.LC20lb span')

    def search_in_google(self, value):
        self.wait_element(*self.SEARCH_FIELD)
        self.find_element(*self.SEARCH_FIELD).send_keys(value + Keys.ENTER)

    def check_results(self, name):
        search = self.find_elements(*self.RESULTS)
        result = search[0].text
        assert result == name

    def click_on_first_result(self):
        results = self.find_elements(*self.RESULTS)
        results[0].click()
