import unittest
import drivers.HTMLTestRunner
import tests.google_test


class MyTestSuite(unittest.TestCase):

    def test_Issue(self):
        smoke_test = unittest.TestSuite()
        smoke_test.addTests([
            unittest.defaultTestLoader.loadTestsFromTestCase(tests.google_test.GoogleTest),
        ])

        outfile = open("C:/Users/Owner/PycharmProjects/armyProject/reports/SmokeTest.html", "wb")

        runner1 = drivers.HTMLTestRunner.HTMLTestRunner(
            stream=outfile,
            title='Test Report',
            description='Smoke Tests'
        )

        runner1.run(smoke_test)


if __name__ == '__main__':
    unittest.main()