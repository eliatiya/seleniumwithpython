import unittest
from utils.base import Base


class BaseTest(unittest.TestCase):
    driver = Base.driver

    @classmethod
    def setUpClass(cls):
        cls.driver.get('https://www.google.com/')
        cls.driver.maximize_window()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

